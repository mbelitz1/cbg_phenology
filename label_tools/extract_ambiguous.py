#!/usr/bin/python3

# Extracts media file names from a CSV file with ambiguous consensus
# annotations.

from argparse import ArgumentParser
import csv


argp = ArgumentParser(
   description='Extracts media file names with ambiguous consensus annotations.'
)
argp.add_argument(
    '-n', '--none_val', type=str, required=False, default='N/A', help='The '
    'value to use for indicating an ambiguous consensus annotation (default: '
    '"N/A").'
)
argp.add_argument(
    '-c', '--colname', type=str, required=True, help='The column of the input '
    'CSV file in which to search for ambiguous annotations.'
)
argp.add_argument(
    'input_file', type=str, help='A consensus annotations file.'
)

args = argp.parse_args()

with open(args.input_file) as fin:
    reader = csv.DictReader(fin)
    for row in reader:
        if args.colname not in row:
            exit(
                '\nERROR: The column "{0}" was not found in {1}.\n'.format(
                    args.colname, args.input_file
                )
            )
        
        if row[args.colname] == args.none_val:
            print(row['file'])

