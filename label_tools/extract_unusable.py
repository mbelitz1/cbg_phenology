#!/usr/bin/python3

# Finds and marks media files in a CSV file of consensus annotations that are
# unusable, either according to majority classification or if any annotator
# marked them as such.

from argparse import ArgumentParser
import csv


argp = ArgumentParser(
   description='Extracts unusable media files in a CSV file of consensus '
   'annotations.  Unusable files will be marked with "1", usable files will '
   'be marked with "0".'
)
argp.add_argument(
    '-u', '--unusable_val', type=str, required=False, default='U', help='The '
    'annotation value that indicates an unusable file (default: "U").'
)
argp.add_argument(
    '-t', '--trait', type=str, required=True, help='The name of the trait to '
    'search in the consensus CSV file.'
)
argp.add_argument(
    '-o', '--output_file', type=str, required=True,
    help='The output file name.'
)
argp.add_argument(
    'input_file', type=str, help='A consensus annotations file.'
)

args = argp.parse_args()

with open(args.input_file) as fin, open(args.output_file, 'w') as fout:
    reader = csv.DictReader(fin)
    writer = csv.DictWriter(
        fout, ['file', 'unusable-consensus', 'unusable-any']
    )
    writer.writeheader()

    colname_m = args.trait + '-majority'
    colname_all = args.trait + '-all'

    rowout = {}

    for row in reader:
        if colname_m not in row or colname_all not in row:
            exit(
                '\nERROR: The columns "{0}" and "{1}" were not found in '
                '{2}.\n'.format(
                    colname_m, colname_all, args.input_file
                )
            )

        rowout['file'] = row['file']

        if row[colname_m] == args.unusable_val:
            rowout['unusable-consensus'] = 1
        else:
            rowout['unusable-consensus'] = 0

        if ('"' + args.unusable_val + '"') in row[colname_all]:
            rowout['unusable-any'] = 1
        else:
            rowout['unusable-any'] = 0

        writer.writerow(rowout)

