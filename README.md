This repository contains tools for bulk downloading image files from a variety of sources.

# Downloading images

## Overview and example

The basic workflow for downloading images from a target image source is to first use the source-specific query tool to generate a CSV file containing image metadata from the target source, then use `src/download_images.py` to actually download the image files.

For example, to download images from iNaturalist for the genus "Cercis", first use `src/get_inat_records.py` to generate a CSV file of relevant image metadata from iNaturalist.  E.g.:
```
$ src/get_inat_records.py Cercis
```
This will create an output file named `iNat_images-Cercis.csv`.

Next, use `src/download_images.py` to download the image files:
```
$ src/download_images.py -i iNat_images-Cercis.csv
```
This will create a directory called `iNat_images-Cercis-raw_images` that contains the downloaded image files, a log file called `iNat_images-Cercis-download_log.csv` that contains information about each downloaded file, and a fail log called `fail_log-DATE-TIME.csv` that contains information about any download attempts that failed.


## Using the iNaturalist query tool

The basic usage of the tool `src/get_inat_records.py` is to provide a single genus name as the main command-line argument; e.g.:
```
src/get_inat_records.py Cercis
```

There are several command-line options that give you more control over what images will be downloaded.  The flag `-r`/`--research_only` means that only research-grade observations will be used.

The flag `-f`/`--full_size` indicates that full-size image files should be downloaded.  By default, images with a maximum dimension of 1,024 pixels will be downloaded; if full-size images are requested, the largest image size available will be downloaded, but note that iNaturalist scales all submitted images to have a maximum dimension of 2,048 pixels.

To get only a count of available image records without actually downloading the image records, use the `-c`/`--counts_only` option.

As a final example, the following command will download metadata for obtaining full-size images for all research-grade observations of Cercis:
```
src/get_inat_records.py -r -f Cercis
```


## Using the Flickr query tool

The basic usage of the tool `src/get_flickr_records.py` is to provide a text search string as the main command-line argument; e.g.:
```
src/get_flickr_records.py Cercis
```

The flag `-f`/`--full_size` indicates that full-size image files should be downloaded.  By default, images with a maximum dimension of 1,024 pixels will be downloaded; if full-size images are requested, the largest image size available will be downloaded.

To get only a count of available image records without actually downloading the image records, use the `-c`/`--counts_only` option.

